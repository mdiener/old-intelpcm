#!/bin/bash

set -o errexit

make

sudo cp *.x /usr/local/bin
cd /usr/local/bin

for f in *.x; do
	sudo setcap cap_sys_rawio+ep $f
	new=$(echo $f | sed s,\.x,,)
	sudo mv $f $new
done

sudo modprobe msr
sudo chmod a+r /sys/firmware/acpi/tables/MCFG
sudo chmod a+rw /dev/mem
sudo chmod a+rw /dev/cpu/*/msr
